import React from 'react';
import Notes from './Notes.jsx';
import uuid from 'node-uuid';
import NoteStore from '../stores/NoteStore.jsx';
import NoteAction from '../actions/NoteAction.jsx';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state =NoteStore.getState();
    }

    render() {
        //this.state =NoteStore.getState();
        const notes = this.state.notes;
         // const notes = NoteStore.getState().notes;
       
        return ( 
            
            <div>
                <button className="add-note" onClick={this.addNote}>+</button>
                <Notes notes = {notes} 
                        onEdit={this.editNote}
                        onDelete={this.deleteNote} />
            </div>
        );
    }

    addNote = () => {
        const notes = this.state.notes;
        console.log("App.addNote nouvelle liste de notes : ", JSON.stringify(notes));
        NoteAction.create({task:'Nouvelle tache'});
        this.setState({notes});
    }

    editNote=(id, task)=>{
        if(!task.trim()){
            return;
        }

        NoteAction.update({id,task});
    }

    deleteNote = (id,e) => {
        e.stopPropagation();
        NoteAction.delete();
   
    }

    componentDidMount(){
        NoteStore.listen(this.storeChanged);
    }
    componentWillUnMount(){
        NoteStore.unlisten(this.storeChanged);
    }

    storeChanged = (state) =>{
        this.setState(state);
    }

}




export default App;