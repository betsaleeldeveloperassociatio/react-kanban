import React from 'react';

//const Note = (props) => <div>{props.task}</div>;

class Note extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            editing : false
        };
    }

    render(){
       // console.log("dans note.render, editing = ", this.state.editing);
        if(this.state.editing){
            return this.renderEdit();
        }
        else return this.renderNote();
     
    };
    renderEdit =()=>{
        return  <input  type="text"
                        ref={
                            (e)=>e?e.selectionStart=this.props.task.length:null
                        }
                        autoFocus={true}
                        
                        onBlur={this.finishEdit}
                        onKeyPress={this.checkEnter} 
                        placeholder={this.props.task} />
    };

    renderNote(){
        const onDelete = this.props.onDelete;

        return ( <div onClick={this.edit}>
        <span className="task">{this.props.task}</span>
        {onDelete ? this.renderDelete():null}
        </div>);
    };
    renderDelete = ()=>{
        return <button className="delete-note" onClick={this.props.onDelete}>x</button>
    }
    edit=()=>{
        this.setState({
            editing : true
        });
    };
    checkEnter=(e)=>{
        if(e.key==='Enter'){
            this.finishEdit(e);
        }
    };
    finishEdit = (e)=>{
        const val = e.target.value;
        if(this.props.onEdit){
            this.props.onEdit(val);

            this.setState({
                editing : false
            });
        }
    }
}
export default Note;
