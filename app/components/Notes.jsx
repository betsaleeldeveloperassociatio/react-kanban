import React from 'react';
import Note from './Note.jsx';

 const Notes = ({notes, onEdit,onDelete})=>{

   console.log("avant le  Notes.render : ");
    notes.map(n=>console.log("note : ",n.task));
    return (
        <ul className="notes">
            {notes.map(n=>
            <li className="note" key={n.id}>
                <Note   task={n.task} 
                        onEdit={onEdit.bind(null,n.id)}
                        onDelete={onDelete.bind(null,n.id)}/>
            </li>
            )}
        </ul>
    );
};
export default Notes;