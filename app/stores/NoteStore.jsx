import uuid from 'node-uuid';
import alt from '../libs/alt';
import NoteAction from '../actions/NoteAction';
class NoteStore {
    constructor(){
        this.bindActions(NoteAction);
        this.notes = [];
    }
    create(note){
         console.log("NoteStore.create, valeur initiale de la note : ", note);
        const notes = this.notes;
        note.id = uuid.v4();
        this.setState({
           notes : notes.concat(note)
        });

        console.log("NoteStore.create: nouvelle liste de notes : ", JSON.stringify(notes));
    }
    update(updatedNote){
        console.log("Dans NoteStore.update",JSON.stringify(this.notes));
        console.log("Dans NoteStore.update, valeur modifiée",JSON.stringify(updatedNote));
        const notes = this.notes.map(n=>{
            if(n.id===updatedNote.id){
                console.log("NoteStore.update: id modifié : ", [n.id, n.task]);
                console.log("NoteStore.update: Nouvelle valeur : ", JSON.stringify(updatedNote));
                return Object.assign({},n,updatedNote);
            }
            return n;
        });
         console.log("NoteSore.update, arpsè mise à jour", notes);
        this.setState({notes});
    }

    delete(id){
        const notes = this.notes.filter(n=>n.id!==id);
        this.setState({notes});
    }
}

export default alt.createStore(NoteStore,'NoteStore');