/**
 * Created by emma on 26/05/17.
 */
const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const TARGET = process.env.npm_lifecycle_event;
const NpmInstallPlugin = require('npm-install-webpack-plugin');
const PATHS = {
    app : path.join(__dirname, 'app'),
    build : path.join(__dirname, 'build')
};
process.env.BABEL_ENV= TARGET;

const common =  {
    entry : {
        app : PATHS.app

    },
    resolve : {
        extensions:['.js','.jsx']
    },
    output : {
        path : PATHS.build,
        filename : 'bundle.js'
    },
    devtool : 'eval-source-map',
    module: {
        loaders : [
            {
                test : /\.jsx?$/,
                query : {
                    cacheDirectory : true,
                    presets : ['react','es2015','survivejs-kanban']
                },
                exclude : /node_modules/,
                loader: 'babel-loader',
                include : PATHS.app
            },
            {
                test : /\.css$/,
               exclude : /node_modules/,
                loader: ['style-loader','css-loader'],
                include : PATHS.app
            }
        ]
    }
};

if(TARGET==='start' || !TARGET){
    module.exports = merge(common, {
        devServer : {
            contentBase : PATHS.build,
            historyApiFallback : true,
            hot : true,
            inline : true,
            stats : 'errors-only',
            host:process.env.HOST,
            port:process.env.PORT
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin(),
            new NpmInstallPlugin({
                dev:true
            })
        ]
    });
}

if(TARGET=='build'){
    module.exports =merge(common,{});
}
